import java.util.Scanner;

public class EchoYourName {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please Input Your Name:");
        String name = sc.next();
        System.out.println("Hello " + name);

    }

}
